// Each of the commands that can be handled
use bytes::Bytes;
use http_body_util::combinators::BoxBody;
use hyper::{body::Incoming, Request, Response};
use std::convert::Infallible;
use tokio::sync::{broadcast, oneshot};

#[derive(Debug)]
pub enum Command {
    Request {
        req: Request<Incoming>,
        sender: oneshot::Sender<Result<Response<BoxBody<Bytes, Infallible>>, Infallible>>,
    },
    Shutdown {
        shutdown_sig: broadcast::Sender<()>,
    },
    Update {},
}
